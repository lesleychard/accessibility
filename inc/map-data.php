<?php
	function export_map($post_id) {
		if( isset( $_POST['post_type'] ) && $_POST['post_type'] == 'location' ) {
		 	$location = array(
		 		'id' => $post_id
		 	);

		 	$fileDir = get_template_directory() . '/js/mapdata.json';

		 	$file = file_get_contents($fileDir);
			$data = json_decode($file);
			if($data == null) {
				$data = array();
			}

			$meta = get_post_meta($post_id);
			$update = false;

			$post = get_post( $post_id );

			$image = null;
			if(has_post_thumbnail($post_id)) {
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			}

			foreach($data as $obj) {
				if($obj->id == $post_id) {
					$obj->latitude = floatval($meta['address-latitude'][0]);
					$obj->longitude = floatval($meta['address-longitude'][0]);
					$obj->class = 'map-poi';
					$obj->name = $post->post_title;
					$obj->photo = $image[0];
					$obj->address->street = $meta['address-street'][0];
					$obj->address->unit = $meta['address-unit'][0];
					$obj->address->city = $meta['address-city'][0];
					$obj->address->province = $meta['address-province'][0];
					$obj->address->postal = $meta['address-postal'][0];
					$obj->address->phone = $meta['address-phone'][0];
					$obj->address->email = $meta['address-email'][0];
					$obj->address->website = $meta['address-website'][0];
					$obj->address->facebook = $meta['address-facebook'][0];
					$obj->address->twitter = $meta['address-twitter'][0];
					$obj->mobility->entrance = filter_var($meta['mobility-entrance'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->mobility->ground = filter_var($meta['mobility-ground'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->mobility->elevators = filter_var($meta['mobility-elevators'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->mobility->fire_escape = filter_var($meta['mobility-fire_escape'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->mobility->surfaces = filter_var($meta['mobility-surfaces'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->mobility->parking = filter_var($meta['mobility-parking'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->mobility->washroom = filter_var($meta['mobility-washroom'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->mobility->signage = filter_var($meta['mobility-signage'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->elevator_braille = filter_var($meta['visual-braille'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->elevator_phone = filter_var($meta['visual-phone'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->elevator_audio = filter_var($meta['visual-audio'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->signage = filter_var($meta['visual-signage'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->steps = filter_var($meta['visual-steps'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->alarms = filter_var($meta['visual-alarms'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->lighting = filter_var($meta['visual-lighting'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->braille = filter_var($meta['visual-braille'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->visual->mirrors = filter_var($meta['visual-mirrors'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->auditory->email = filter_var($meta['auditory-email'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->auditory->alarms = filter_var($meta['auditory-alarms'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->auditory->queues = filter_var($meta['auditory-queues'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->auditory->wide_areas = filter_var($meta['auditory-wide_areas'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->auditory->lighting = filter_var($meta['auditory-lighting'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->auditory->acoustics = filter_var($meta['auditory-acoustics'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->other->pictograms = filter_var($meta['other-pictograms'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->other->directory = filter_var($meta['other-directory'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->other->assistance = filter_var($meta['other-assistance'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$obj->other->signage = filter_var($meta['other-signage'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
					$update = true;
				}
			}

			if(!$update) {
				$newObj = (object) array(
					'id' => $post_id,
					'latitude' => floatval($meta['address-latitude'][0]),
					'longitude' => floatval($meta['address-longitude'][0]),
					'class' => 'map-poi',
					'name' => $post->post_title,
					'photo' => $image[0],
					'address' => array(
						'street' => $meta['address-street'][0],
						'unit' => $meta['address-unit'][0],
						'city' => $meta['address-city'][0],
						'province' => $meta['address-province'][0],
						'postal' => $meta['address-postal'][0],
						'phone' => $meta['address-phone'][0],
						'email' => $meta['address-email'][0],
						'website' => $meta['address-website'][0],
						'facebook' => $meta['address-facebook'][0],
						'twitter' => $meta['address-twitter'][0]
					),
					'mobility' => array(
						'entrance' => filter_var($meta['mobility-entrance'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'ground' => filter_var($meta['mobility-ground'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'elevators' => filter_var($meta['mobility-elevators'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'fire_escape' => filter_var($meta['mobility-fire_escape'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'surfaces' => filter_var($meta['mobility-surfaces'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'parking' => filter_var($meta['mobility-parking'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'washroom' => filter_var($meta['mobility-washroom'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'signage' => filter_var($meta['mobility-signage'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
					),
					'visual' => array(
						'elevator_braille' => filter_var($meta['visual-braille'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'elevator_phone' => filter_var($meta['visual-phone'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'elevator_audio' => filter_var($meta['visual-audio'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'signage' => filter_var($meta['visual-signage'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'steps' => filter_var($meta['visual-steps'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'alarms' => filter_var($meta['visual-alarms'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'lighting' => filter_var($meta['visual-lighting'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'braille' => filter_var($meta['visual-braille'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'mirrors'  => filter_var($meta['visual-mirrors'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
					),
					'auditory' => array(
						'email' => filter_var($meta['auditory-email'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'alarms' => filter_var($meta['auditory-alarms'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'queues' => filter_var($meta['auditory-queues'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'wide_areas' => filter_var($meta['auditory-wide_areas'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'lighting' => filter_var($meta['auditory-lighting'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'acoustics '=> filter_var($meta['auditory-acoustics'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
					),
					'other' => array(
						'pictograms' => filter_var($meta['other-pictograms'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'directory' => filter_var($meta['other-directory'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'assistance' => filter_var($meta['other-assistance'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
						'signage' =>filter_var($meta['other-signage'][0], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)
					)
				);
				array_push($data, $newObj);
			}

			$newData = json_encode($data);
			file_put_contents($fileDir, $newData);
		}
	}
	add_action( 'save_post', 'export_map' );
?>