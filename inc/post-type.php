<?php
	//Register Post Types
	add_action( 'init', 'create_post_type' );
	function create_post_type() {
		register_post_type( 'location',
			array(
				'labels' => array(
					'name' => __( 'Locations' ),
					'singular_name' => __( 'Location' )
				),
				'public' => true,
				'has_archive' => true,
				'supports' => array(
					'title',
					'thumbnail'
				)
			)
		);
	}

	function location_add_meta_box() {
		add_meta_box(
			'location_properties',
			__( 'Location Properties', 'location_properties' ),
			'location_meta_box_callback',
			'location'
		);
	}
	add_action( 'add_meta_boxes', 'location_add_meta_box' );

	function location_meta_box_callback( $post ) {

		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'location_meta_box', 'location_meta_box_nonce' );

		$m = get_post_meta( $post->ID );
		?>
		<div class="cool-form">
			<fieldset id="address">
				<legend>Location &amp; Address</legend>
				<p>
					<em>In order for us to find the location, we need the latitude and logitude or the complete street location</em>
				</p>
				<div class="row">
					<div class="col-50">
						<label for="address-street">Street Address
							<input type="text" id="address-street" name="address-street" value="<?php echo $m['address-street'][0] ?>" />
						</label>
						<label for="address-unit">Unit #
							<input type="text" id="address-unit" name="address-unit" value="<?php echo $m['address-unit'][0] ?>" />
						</label>
						<label for="address-city">City
							<input type="text" id="address-city" name="address-city" value="<?php echo $m['address-city'][0] ?>" />
						</label>
						<label for="address-province">Province
							<input type="text" id="address-province" name="address-province" value="<?php echo $m['address-province'][0] ?>" />
						</label>
						<label for="address-postal">Postal Code
							<input type="text" id="address-postal" name="address-postal" value="<?php echo $m['address-postal'][0] ?>" />
						</label>
					</div>
					<div class="col-50">
						<label for="address-latitude">Latitude
							<input type="number" id="address-latitude" name="address-latitude" value="<?php echo $m['address-latitude'][0] ?>" step="0.0000001" />
						</label>
						<label for="address-longitude">Longitude
							<input type="number" id="address-longitude" name="address-longitude" value="<?php echo $m['address-longitude'][0] ?>" step="0.0000001" />
						</label>
					</div>
				</div>
			</fieldset>
			<fieldset id="social">
				<legend>Contact Information</legend>
				<div class="row">
					<div class="col-50">
						<label for="address-phone">Phone Number
							<input type="text" id="address-phone" name="address-phone" value="<?php echo $m['address-phone'][0] ?>" />
						</label>
						<label for="address-email">Email Address
							<input type="text" id="address-email" name="address-email" value="<?php echo $m['address-email'][0] ?>" />
						</label>
						<label for="address-website">Website
							<input type="text" id="address-website" name="address-website" value="<?php echo $m['address-website'][0] ?>" />
						</label>
					</div>
					<div class="col-50">
						<label for="address-facebook">Facebook URL
							<input type="text" id="address-facebook" name="address-facebook" value="<?php echo $m['address-facebook'][0] ?>" />
						</label>
						<label for="address-twitter">Twitter URL
							<input type="text" id="address-twitter" name="address-twitter" value="<?php echo $m['address-twitter'][0] ?>" />
						</label>
					</div>
				</div>
			</fieldset>
			<fieldset id="mobility">
				<legend>Physical Disabilties and Assisted Mobility</legend>
				<div class="row">
					<div class="col-50">
						<label for="mobility-entrance">Entrance has a ramp and automatic door.
							<select id="mobility-entrance" name="mobility-entrance">
								<option value="null" <?php if($m['mobility-entrance'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['mobility-entrance'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['mobility-entrance'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="mobility-ground">Floor is smooth with no obstructions and halls are wide.
							<select id="mobility-ground" name="mobility-ground">
								<option value="null" <?php if($m['mobility-ground'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['mobility-ground'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['mobility-ground'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="mobility-elevators">Elevators are sufficiently wide to fit a wheelchair.
							<select id="mobility-elevators" name="mobility-elevators">
								<option value="null" <?php if($m['mobility-elevators'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['mobility-elevators'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['mobility-elevators'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="mobility-fire_escape">Accessible fire escapes.
							<select id="mobility-fire_escape" name="mobility-fire_escape">
								<option value="null" <?php if($m['mobility-fire_escape'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['mobility-fire_escape'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['mobility-fire_escape'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
					</div>
					<div class="col-50">
						<label for="mobility-surfaces">Counters, tables and other surfaces are low enough to be reached.
							<select id="mobility-surfaces" name="mobility-surfaces">
								<option value="null" <?php if($m['mobility-surfaces'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['mobility-surfaces'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['mobility-surfaces'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="mobility-parking">Accessible parking spots available.
							<select id="mobility-parking" name="mobility-parking">
								<option value="null" <?php if($m['mobility-parking'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['mobility-parking'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['mobility-parking'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="mobility-washroom">Accessible washroom or accessible stalls available.
							<select id="mobility-washroom" name="mobility-washroom">
								<option value="null" <?php if($m['mobility-washroom'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['mobility-washroom'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['mobility-washroom'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="mobility-signage">All signage is placed sufficiently low to be visible from wheelchair.
							<select id="mobility-signage" name="mobility-signage">
								<option value="null" <?php if($m['mobility-signage'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['mobility-signage'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['mobility-signage'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
					</div>
				</div>
			</fieldset>
			<fieldset id="visual">
				<legend>Blind and Low Vision</legend>
				<div class="row">
					<div class="col-50">
						<label for="visual-elevator_braille">Braille signage in elevator.
							<select id="visual-elevator_braille" name="visual-elevator_braille">
								<option value="null" <?php if($m['visual-elevator_braille'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-elevator_braille'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-elevator_braille'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="visual-elevator_phone">Two-way emergency phone in elevator.
							<select id="visual-elevator_phone" name="visual-elevator_phone">
								<option value="null" <?php if($m['visual-elevator_phone'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-elevator_phone'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-elevator_phone'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="visual-elevator_audio">Audible signals in elevator for floor number and direction.
							<select id="visual-elevator_audio" name="visual-elevator_audio">
								<option value="null" <?php if($m['visual-elevator_audio'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-elevator_audio'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-elevator_audio'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="visual-signage">Signage with high contrast and large font size.
							<select id="visual-signage" name="visual-signage">
								<option value="null" <?php if($m['visual-signage'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-signage'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-signage'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
					</div>
					<div class="col-50">
						<label for="visual-steps">Steps marked with contrasting tape.
							<select id="visual-steps" name="visual-steps">
								<option value="null" <?php if($m['visual-steps'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-steps'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-steps'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="visual-alarms">Audible alarm signals in emergency system.
							<select id="visual-alarms" name="visual-alarms">
								<option value="null" <?php if($m['visual-alarms'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-alarms'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-alarms'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="visual-lighting">Adequately bright lighting.
							<select id="visual-lighting" name="visual-lighting">
								<option value="null" <?php if($m['visual-lighting'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-lighting'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-lighting'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="visual-braille">Braille information available.
							<select id="visual-braille" name="visual-braille">
								<option value="null" <?php if($m['visual-braille'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-braille'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-braille'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="visual-mirrors">Large mirrors are marked with sticker(s).
							<select id="visual-mirrors" name="visual-mirrors">
								<option value="null" <?php if($m['visual-mirrors'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['visual-mirrors'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['visual-mirrors'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
					</div>
				</div>
			</fieldset>
			<fieldset id="auditory">
				<legend>Deaf and Low Hearing</legend>
				<div class="row">
					<div class="col-50">
						<label for="auditory-email">Email or text message communication available.
							<select id="auditory-email" name="auditory-email">
								<option value="null" <?php if($m['auditory-email'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['auditory-email'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['auditory-email'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="auditory-alarms">Alarm system has visual emergency signals.
							<select id="auditory-alarms" name="auditory-alarms">
								<option value="null" <?php if($m['auditory-alarms'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['auditory-alarms'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['auditory-alarms'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="auditory-queues">Queues rely on both audio and visual signals.
							<select id="auditory-queues" name="auditory-queues">
								<option value="null" <?php if($m['auditory-queues'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['auditory-queues'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['auditory-queues'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
					</div>
					<div class="col-50">
						<label for="auditory-wide_areas">Wide halls and areas for signers.
							<select id="auditory-wide_areas" name="auditory-wide_areas">
								<option value="null" <?php if($m['auditory-wide_areas'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['auditory-wide_areas'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['auditory-wide_areas'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="auditory-lighting">Proper lighting to minimize visual strain.
							<select id="auditory-lighting" name="auditory-lighting">
								<option value="null" <?php if($m['auditory-lighting'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['auditory-lighting'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['auditory-lighting'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="auditory-acoustics">Rooms have adequate acoustics.
							<select id="auditory-acoustics" name="auditory-acoustics">
								<option value="null" <?php if($m['auditory-acoustics'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['auditory-acoustics'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['auditory-acoustics'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
					</div>
				</div>
			</fieldset>
			<fieldset id="other">
				<legend>Learning Disabilities and Other</legend>
				<div class="row">
					<div class="col-50">
						<label for="other-pictograms">Signs include pictograms and symbols.
							<select id="other-pictograms" name="other-pictograms">
								<option value="null" <?php if($m['other-pictograms'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['other-pictograms'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['other-pictograms'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="other-directory">Building directory is available and centrally located.
							<select id="other-directory" name="other-directory">
								<option value="null" <?php if($m['other-directory'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['other-directory'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['other-directory'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
					</div>
					<div class="col-50">
						<label for="other-assistance">Assistance is available and centrally located.
							<select id="other-assistance" name="other-assistance">
								<option value="null" <?php if($m['other-assistance'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['other-assistance'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['other-assistance'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
						<label for="other-signage">Signage has large and bold lettering.
							<select id="other-signage" name="other-signage">
								<option value="null" <?php if($m['other-signage'][0] == "null") echo "selected" ?>>Not Applicable</option>
								<option value="true" <?php if($m['other-signage'][0] == "true") echo "selected" ?>>Yes</option>
								<option value="false" <?php if($m['other-signage'][0] == "false") echo "selected" ?>>No</option>
							</select>
						</label>
					</div>
				</div>
			</fieldset>
		</div>
		<?php
	}

	function myplugin_save_meta_box_data( $post_id ) {

		// Check if our nonce is set.
		if ( ! isset( $_POST['location_meta_box_nonce'] ) ) {
			return;
		}

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['location_meta_box_nonce'], 'location_meta_box' ) ) {
			return;
		}

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check the user's permissions.
		if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return;
			}

		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
		}

		/* OK, it's safe for us to save the data now. */

		$keys = array(
			'address-latitude',
			'address-longitude',
			'address-street',
			'address-unit',
			'address-city',
			'address-province',
			'address-postal',
			'address-phone',
			'address-email',
			'address-website',
			'address-facebook',
			'address-twitter',
			'mobility-entrance',
			'mobility-ground',
			'mobility-elevators',
			'mobility-fire_escape',
			'mobility-surfaces',
			'mobility-parking',
			'mobility-washroom',
			'mobility-signage',
			'visual-elevator_braille',
			'visual-elevator_phone',
			'visual-elevator_audio',
			'visual-signage',
			'visual-steps',
			'visual-alarms',
			'visual-lighting',
			'visual-braille',
			'visual-mirrors',
			'auditory-email',
			'auditory-alarms',
			'auditory-queues',
			'auditory-wide_areas',
			'auditory-lighting',
			'auditory-acoustics',
			'other-pictograms',
			'other-directory',
			'other-assistance',
			'other-signage'
		);

		foreach($keys as $key) {
			// Sanitize user input.
			$new_meta_value = ( isset( $_POST[$key] ) ? sanitize_text_field( $_POST[$key] ) : '' );

			// Update the meta field in the database.
			update_post_meta( $post_id, $key, $new_meta_value );
		}
	}

		
	add_action( 'save_post', 'myplugin_save_meta_box_data' );