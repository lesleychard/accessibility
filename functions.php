<?php
/**
 * Accessibility functions and definitions
 *
 * @package Accessibility
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'access_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function access_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Accessibility, use a find and replace
	 * to change 'access' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'access', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'access' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'access_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // access_setup
add_action( 'after_setup_theme', 'access_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function access_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'access' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'access_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function access_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css' );

	wp_enqueue_script( 'access-jquery', get_template_directory_uri() . '/js/jquery.js', array(), '1.0', true );
	wp_enqueue_script( 'handlebars', get_template_directory_uri() . '/js/handlebars.js', array(), '1.0', true );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array(), '1.0', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0', true );
	wp_enqueue_script( 'slimscroll', get_template_directory_uri() . '/js/slimscroll.min.js', array(), '1.0', true );
	wp_enqueue_script( 'gmaps', "https://maps.googleapis.com/maps/api/js?key=AIzaSyC925yJ_WbaJoZGiEBXmU_x6xErpBsgmKQ", array(), '1.0', true );
	wp_enqueue_script( 'elabel', get_template_directory_uri() . '/js/elabel.js', array(), '1.0', true );
	wp_enqueue_script( 'map', get_template_directory_uri() . '/js/map.js', array(), '1.0', true );
	wp_enqueue_script( 'hotkeys', get_template_directory_uri() . '/js/hotkeys.js', array(), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'access_scripts' );

function load_custom_wp_admin_style() {
        wp_register_style( 'custom_admin_css', get_template_directory_uri() . '/css/admin.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

require get_template_directory() . '/inc/post-type.php';
require get_template_directory() . '/inc/map-data.php';

add_action('wp_ajax_location_search', 'location_search');
add_action('wp_ajax_nopriv_location_search', 'location_search');

function location_search() {
    $query = $_POST['search_string'];
    
    $args = array(
        'post_type' => 'location',
        'post_status' => 'publish',
        's' => $query
    );
    $search = new WP_Query( $args );
    
    if ( $search->have_posts() ) {

    	foreach($search->posts as $res) {
    		$meta = get_post_meta($res->ID);
    	?>

		<div class="row">
			<?php if(has_post_thumbnail($res->ID)) { ?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $res->ID ), 'single-post-thumbnail' ); ?>
				<div class="col-md-4">
					<img src="<?php echo $image[0]; ?>" class="img-responsive img-featured" alt="Photo of <?php echo $res->post_title; ?>" />
				</div><!-- .col -->
			<?php } ?>
			<div class="col-md-8">
				<h1><a href="#" class="search-post" data-id="<?php echo $res->ID ?>"><?php echo $res->post_title; ?></a></h1>
				<address>
					<?php echo $meta['address-street'][0] ?>,<br />
					<?php echo $meta['address-city'][0] ?> <?php echo $meta['address-province'][0] ?>, <?php echo $meta['address-postal'][0] ?><br />
				</address>
			</div><!-- .col -->
		</div><!-- .row -->

		<?php
		}

	} else {

		echo 'No results found.';
		
	}

	die();	
}