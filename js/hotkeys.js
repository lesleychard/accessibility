//activate hot keys
var hotFlag = false;

$(document).ready(function(){
	//show/hide hot key drop-down
	$("#hotkey-toggle").click(function(e){
		e.preventDefault();
		$('.hot-keys').toggleClass("show");
	});

	//list of hot keys
	var hotkeys = [
		["X", "Show/Hide Hot Keys"],
		["H", "Help"],
		["S", "Search"]
	];

	//append hot key list to layout
	for(var i = 0; i < hotkeys.length; i++) {
		var col = (i % 3) + 1;
		$('#hot-keys-' + col).append('<span hotkey="' + hotkeys[i][0] + '">' + hotkeys[i][1] + '</span>');
	}

	var map = [];
	var downFlag = false; //true if H and K are both down

	onkeydown = onkeyup = function(e){
	    e = e || event; // to deal with IE
	    map[e.keyCode] = e.type == 'keydown';
	    
	    if(!hotFlag) { //if hot keys are disabled
		    if(map[72] && map[75]) { //if H and K are both down
		    	downFlag = true; //set down flag
		    }
		    //if keys were both down, now they're both up
		    if(downFlag && !map[72] && !map[75]) {
		    	//enable hot keys
		    	hotFlag = true;
		    	$('<div class="alert alert-warning" id="hotkey-alert"><b>Attention:</b> Hot keys have been enabled. Press "H" and "K" simultaneously to disable hot keys.</div>').appendTo($('#message-area')).slideDown('fast');
		    	$('.hot-keys').addClass("show");
		    }
		} else { //if hot keys are enabled
			//if H and K are pressed
			if(map[72] && map[75]) {
				//disable hot keys, reset down flag
				hotFlag = downFlag = false;
				$('#hotkey-alert').slideUp('fast', function(){
					$(this).remove();
				});
			}
		}
	}
});

/* Deals with hot key functions */
$(document).on('keyup', function(e) {
	console.log(hotFlag);
	//if hot keys are enabled
	if(hotFlag) {
	    var tag = e.target.tagName.toLowerCase();
	    if(tag != 'input' && tag != 'textarea') {
	    	console.log(e.which);
	       switch(e.which) {
	       		//case H
	       		case 72:
	       			$("#help-button").click();
	       			break;
	       		//case S
	       		case 83:
	       			$("#search").focus();
	       			break;
	       		//case X:
	       		case 88:
	       			$('.hot-keys').toggleClass("show");
	       			break;
	       }
	    }
	}
});