var map;

var mapStyle = {
	"marker" : 30,
	"road" : 1,
	//"hue" : "#4ecdc4",
	"saturation" : 0,
	"lightness" : 0,
	"invert_lightness" : false,
	"gamma" : 0
}

/* Initialize Map */
function initialize() {
	var mapCanvas = document.getElementById('map');
	var mapOptions = {
		center: new google.maps.LatLng(47.5624459,-52.7357382),
		zoom: 12,
		mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(mapCanvas, mapOptions)

    styleMap(mapStyle);

    //Get map data
	$.getJSON("http://lesleychard.com/access/wp-content/themes/access/js/mapdata.json", function(data){
		setPoints(data);
	});
}

/* Set points from JSON file */
function setPoints(points) {
	for(var i = 0; i < points.length; i++) {
		var label = new ELabel({
			latlng: new google.maps.LatLng(points[i].latitude,points[i].longitude), 
			label: '<a href="#">&#xf041;</a>', 
			classname: points[i].class, 
			offset: 0, 
			opacity: 100, 
			overlap: true,
			pointProps: {
				id: points[i].id,
				name: points[i].name,
				photo: points[i].photo,
				address: points[i].address,
				mobility: points[i].mobility,
				visual: points[i].visual,
				auditory: points[i].auditory,
				other: points[i].other
			}
		});
		label.setMap(map);
	}
}

function enableSheet(title) {
	$('#main-sheet').remove();
	$('head').append('<link href="css/style-' + title + '.css" rel="stylesheet" id="main-sheet" />');
}

function styleMap(myMapStyle) {
	map.set('styles', [
		{
			stylers: [
				{ "hue" : myMapStyle.hue },
				{ "saturation" : myMapStyle.saturation },
				{ "lightness" : myMapStyle.lightness },
				{ "invert_lightness" : myMapStyle.invert_lightness },
				{ "gamma" : myMapStyle.gamma }
			]
		},
		{
			featureType: 'road',
			elementType: 'geometry',
			stylers: [
				{ weight: myMapStyle.road }
			]
		}
	]);
}

$("#control-font").on("change", function(){
	$("body").css("fontSize", $(this).val() + "em");
});

$("#control-contrast").on('change', function(){
	contrast = parseInt($(this).val());
	console.log(contrast);
	switch(contrast) {
		case 1:
			mapStyle.saturation = -100;
			mapStyle.lightness = 0;
			mapStyle.invert_lightness = false;
			break;
		case 2:
			mapStyle.saturation = 0;
			mapStyle.lightness = 0;
			mapStyle.invert_lightness = false;
			break;
		case 3:
			mapStyle.saturation = -20;
			mapStyle.lightness = -50;
			mapStyle.invert_lightness = false;
			break;
		case 4:
			mapStyle.saturation = -20;
			mapStyle.lightness = 50;
			mapStyle.invert_lightness = true;	
			break;
		case 5:
			mapStyle.saturation = -20;
			mapStyle.lightness = 0;
			mapStyle.invert_lightness = true;	
			break;
		case 6:
			mapStyle.saturation = -100;
			mapStyle.lightness = 0;
			mapStyle.invert_lightness = true;
			break;
	}
	styleMap(mapStyle);
	$("body").removeClass().addClass('body-c' + contrast);
});

/* Asjust Map Marker Size */
$("#control-markers").on('change', function(){
	mapStyle.marker = parseInt($(this).val());
	$('.map-poi a').each(function(){
		$(this).css({
			"fontSize" : mapStyle.marker + "px"
		});
	});

});

/* Adjust Road Size */
$('#control-roads').on('change', function(){
	road = parseInt($(this).val());
	mapStyle.road = road;
	styleMap(mapStyle);
});

google.maps.event.addDomListener(window, 'load', initialize);