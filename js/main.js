$(function(){
    $('.content-left .content-left-container').slimScroll({
        height: '100%'
    });
});

$(document).ready(function(){
	$(".accordion .accordion-header").click(function(){
		var expanded = $(this).parent().hasClass("expanded");
		$(".accordion .accordion-section").removeClass('expanded');
		if(!expanded) {
			$(this).parent().addClass('expanded');
		}
	});
});

$(document).on("click", ".summary-block", function(e){
	e.preventDefault();
	var target = '#' + $(this).data('target');

	$('.summary-block').removeClass('selected');
	$(this).addClass('selected');

	$('.tabs-item').removeClass('active');
	$(target).addClass('active');
});

$(document).ready(function(){
    $("#search-form").submit(function(e){
        e.preventDefault();
        $("#content-area, #web-area").slideUp("fast");
        var searchVal = $("#search").val(); 
        $.ajax({
            type:"POST",
            url: "./wp-admin/admin-ajax.php",
            data: {
                action:'location_search', 
                search_string : searchVal
            },
            success:function(data){
                $('#web-area').html(data).fadeIn('fast');
            }
   	 	});  
   	}); 
});



$(document).on("click", ".search-post", function(e){
	e.preventDefault();
	var id = $(this).data('id');
	$.getJSON("http://lesleychard.com/access/wp-content/themes/access/js/mapdata.json", function(data){
		for(var i = 0; i < data.length; i++) {
			if(data[i]['id'] == id) {
				$("#web-area").slideUp('fast');
				var counts = tallyCounts(data[i]);

				var context = data[i];
				var html = template(context);
				$("#content-area").html(html).fadeIn("fast");

				for(var key in counts) {
					$("#" + key + " .total-check").html(counts[key].check);
					$("#" + key + " .total-appl").html(counts[key].appl);
					$("#" + key + " .total-score").html(counts[key].score + "%");
					$(".summary-block-" + key + " .amount").html(counts[key].score + "%");
				}

			    break;
			}
		}
	});
});