<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Accessibility
 */

get_header(); ?>

<div class="container-fluid content">
	<div class="row content-row">
		<div class="col-md-4 content-left">
			<div class="content-left-container">
				<button class="main-help" id="help-button" data-toggle="modal" data-target="#help">Help</button>

				<form class="search-form" id="search-form">
					<input type="text" id="search" placeholder="Search for locations..." name="s" />
					<button>Search</button>
				</form>

				<div id="message-area">
				</div>

				<div id="web-area">
					<div class="text-center">
						<h1>Welcome to the St. John's Accessibility Map!</h1> 
						<p>
							To find the accessibility score for a location, use the search above or click on a map marker.
						</p>
					</div>
				</div>

				<div id="content-area">
					<h1>{{name}}</h1>
					<div class="row">
						{{#if photo}}
						<div class="col-md-5">
							<img src="{{photo}}" class="img-responsive img-featured" alt="Photo of {{name}}" />
						</div><!-- .col -->
						{{/if}}
						<div class="col-md-7">
							<address>
								{{address.street}},<br />
								{{address.city}}, {{address.province}}, {{address.postal}}<br />
								{{address.phone}}<br />
								<a href="#">{{address.email}}</a>
							</address>
						</div><!-- .col -->
					</div><!-- .row -->
					
					<div class="content-connect">
						<strong>Connect: </strong>
						{{#if address.website}}<a href="{{address.website}}">Website</a>{{/if}}
						{{#if address.facebook}}<a href="{{address.facebook}}">Facebook</a>{{/if}}
						{{#if address.twitter}}<a href="{{address.twitter}}">Twitter</a>{{/if}}
					</div><!-- .content-connect -->

					<h2>Accessibility Rating</h2>
					<div class="content-summary">
						<div class="clearfix">
							<a href="#" class="summary-block summary-block-mobility selected" data-target="mobility">
								<span class="title">Physical Disabilities and Assisted Mobility</span>
								<span class="amount">0%</span>
							</a>
							<a href="#" class="summary-block summary-block-visual" data-target="visual">
								<span class="title">Blind and Low Vision</span>
								<span class="amount">0%</span>
							</a>
							<a href="#" class="summary-block summary-block-auditory" data-target="auditory">
								<span class="title">Deaf and Low Hearing</span>
								<span class="amount">0%</span>
							</a>
							<a href="#" class="summary-block summary-block-other" data-target="other">
								<span class="title">Learning Disabilities and Other</span>
								<span class="amount">0%</span>
							</a>
						</div>
						<div class="tabs">
							<div class="tabs-item active" id="mobility">
								<h3>Physical Disabilities and Assisted Mobility</h3>
								<ul class="points">
									<li class="question question-{{mobility.entrance}}">
										Entrance has a ramp and automatic door.
										<span class="answer">{{mobility.entrance}}</span>
									</li>
									<li class="question question-{{mobility.ground}}">
										Floor is smooth with no obstructions and halls are wide.
										<span class="answer">{{mobility.ground}}</span>
									</li>
									<li class="question question-{{mobility.elevators}}">
										Elevators are sufficiently wide to fit a wheelchair.
										<span class="answer">{{mobility.elevators}}</span>
									</li>
									<li class="question question-{{mobility.fire_escape}}">
										Accessible fire escapes.
										<span class="answer">{{mobility.fire_escape}}</span>
									</li>
									<li class="question question-{{mobility.surfaces}}">
										Counters, tables and other surfaces are low enough to be reached.
										<span class="answer">{{mobility.surfaces}}</span>
									</li>
									<li class="question question-{{mobility.parking}}">
										Accessible parking spots available.
										<span class="answer">{{mobility.parking}}</span>
									</li>
									<li class="question question-{{mobility.washroom}}">
										Accessible washroom or accessible stalls available.
										<span class="answer">{{mobility.washroom}}</span>
									</li>
									<li class="question question-{{mobility.signage}}">
										All signage is placed sufficiently low to be visible from wheelchair.
										<span class="answer">{{mobility.signage}}</span>
									</li>
								</ul>
								<table class="points-score">
									<tbody>
										<tr>
											<td>Total checked:</td>
											<td class="total-check"></td>
										</tr>
										<tr>
											<td>Total applicable:</td>
											<td class="total-appl"></td>
										</tr>
										<tr>
											<td>Final Score:</td>
											<td class="total-score"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="tabs-item" id="visual">
								<h3 class="vision">Blind and Low Vision</h3>
								<ul class="points">
									<li class="question question-{{visual.elevator_braille}}">
										Braille signage in elevator.
										<span class="answer">{{visual.elevator_braille}}</span>
									</li>
									<li class="question question-{{visual.elevator_phone}}">
										Two-way emergency phone in elevator.
										<span class="answer">{{visual.elevator_phone}}</span>
									</li>
									<li class="question question-{{visual.elevator_audio}}">
										Audible signals in elevator for floor number and direction.
										<span class="answer">{{visual.elevator_audio}}</span>
									</li>
									<li class="question question-{{visual.signage}}">
										Signage with high contrast and large font size.
										<span class="answer">{{visual.signage}}</span>
									</li>
									<li class="question question-{{visual.steps}}">
										Steps marked with contrasting tape.
										<span class="answer">{{visual.steps}}</span>
									</li>
									<li class="question question-{{visual.alarms}}">
										Audible alarm signals in emergency system.
										<span class="answer">{{visual.alarms}}</span>
									</li>
									<li class="question question-{{visual.lighting}}">
										Adequately bright lighting.
										<span class="answer">{{visual.lighting}}</span>
									</li>
									<li class="question question-{{visual.braille}}">
										Braille information available.
										<span class="answer">{{visual.braille}}</span>
									</li>
									<li class="question question-{{visual.mirrors}}">
										Large mirrors are marked with sticker(s).
										<span class="answer">{{visual.mirrors}}</span>
									</li>
								</ul>
								<table class="points-score">
									<tbody>
										<tr>
											<td>Total checked:</td>
											<td class="total-check"></td>
										</tr>
										<tr>
											<td>Total applicable:</td>
											<td class="total-appl"></td>
										</tr>
										<tr>
											<td>Final Score:</td>
											<td class="total-score"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="tabs-item" id="auditory">
								<h3 class="hearing">Deaf and Low Hearing</h3>
								<ul class="points">
									<li class="question question-{{auditory.email}}">
										Email or text message communication available.
										<span class="answer">{{auditory.email}}</span>
									</li>
									<li class="question question-{{auditory.alarms}}">
										Alarm system has visual emergency signals.
										<span class="answer">{{auditory.alarms}}</span>
									</li>
									<li class="question question-{{auditory.queues}}">
										Queues rely on both audio and visual signals.
										<span class="answer">{{auditory.queues}}</span>
									</li>
									<li class="question question-{{auditory.wide_areas}}">
										Wide halls and areas for signers.
										<span class="answer">{{auditory.wide_areas}}</span>
									</li>
									<li class="question question-{{auditory.lighting}}">
										Proper lighting to minimize visual strain.
										<span class="answer">{{auditory.lighting}}</span>
									</li>
									<li class="question question-{{auditory.acoustics}}">
										Rooms have adequate acoustics.
										<span class="answer">{{auditory.acoustics}}</span>
									</li>
								</ul>
								<table class="points-score">
									<tbody>
										<tr>
											<td>Total checked:</td>
											<td class="total-check"></td>
										</tr>
										<tr>
											<td>Total applicable:</td>
											<td class="total-appl"></td>
										</tr>
										<tr>
											<td>Final Score:</td>
											<td class="total-score"></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="tabs-item" id="other">
								<h3 class="other">Learning Disabilities and Other</h3>
								<ul class="points">
									<li class="question question-{{other.pictograms}}">
										Signs include pictograms and symbols.
										<span class="answer">{{other.pictograms}}</span>
									</li>
									<li class="question question-{{other.directory}}">
										Building directory is available and centrally located.
										<span class="answer">{{other.directory}}</span>
									</li>
									<li class="question question-{{other.assistance}}">
										Assistance is available and centrally located.
										<span class="answer">{{other.assistance}}</span>
									</li>
									<li class="question question-{{other.signage}}">
										Signage has large and bold lettering.
										<span class="answer">{{other.signage}}</span>
									</li>
								</ul>
								<table class="points-score">
									<tbody>
										<tr>
											<td>Total checked:</td>
											<td class="total-check"></td>
										</tr>
										<tr>
											<td>Total applicable:</td>
											<td class="total-appl"></td>
										</tr>
										<tr>
											<td>Final Score:</td>
											<td class="total-score"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>			
				</div><!-- #content-area -->				
			</div><!-- .container -->
		</div><!-- .content-left -->
		<div class="col-md-8 content-right">
			<nav>
				<div class="row-fluid">
					<div class="col-md-3">
						<button class="button button-hotkeys" id="hotkey-toggle">Hot Keys</button>
					</div><!-- .col -->
					<div class="col-md-6 col-md-offset-3">
						<div class="row">
							<div class="col-md-6 control-area">
								<label for="control-font" class="range-label">Change Font Size</label>
								<input type="range" name="control-font" id="control-font" min="1.7" max="4" step="0.5" value="1" />
								<br />
								<label for="control-markers" class="range-label">Change Map Marker Size</label>
								<input type="range" name="control-markers" id="control-markers" min="30" max="60" step="5" value="30" />
							</div><!-- .col -->
							<div class="col-md-6 control-area">
								<label for="control-contrast" class="range-label">Change Contrast Level</label>
								<input type="range" name="control-contrast" id="control-contrast" min="1" max="6" step="1" value="2" />
								<br />
								<label for="control-roads" class="range-label">Change Road Size</label>
								<input type="range" name="control-roads" id="control-roads" min="1" max="10" step="1" value="1" />
							</div><!-- .col -->
						</div><!-- .row -->
					</div><!-- .col -->
				</div><!-- .row -->
			</nav>
			<div class="hot-keys">
				<p>Simultaneously hold down "H" and "K" on your keyboard to activate hot keys.</p>
				<div class="row">
					<div class="col-md-3" id="hot-keys-1"></div><!-- .col -->
					<div class="col-md-3" id="hot-keys-2"></div><!-- .col -->
					<div class="col-md-3" id="hot-keys-3"></div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .hot-keys -->

			<div id="map"></div>
		</div><!-- .content-right -->
	</div><!-- .content-row -->
</div><!-- .content -->

<div class="modal fade" id="help" tabindex="-1" role="dialog" aria-labelledby="helpLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="helpLabel">Help</h4>
			</div><!-- .modal-header -->
			<div class="modal-body">
				<?php 
					$help = get_post(7); 
					echo apply_filters("the_content", $help->post_content);
				?>
			</div><!-- .modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div><!-- .modal-footer -->
		</div><!-- .modal-content -->
	</div><!-- .modal-dialog -->
</div><!-- .modal -->

<script>
	var templateURL = <?php echo get_template_directory(); ?>;
</script>

<?php get_footer(); ?>
